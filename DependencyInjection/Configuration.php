<?php

namespace Nitra\BuyerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder();
        $builder->root('nitra_buyer')
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('phone_mask')->defaultValue('+38(999)999-99-99')->end()
            ->end()
        ;

        return $builder;
    }
}