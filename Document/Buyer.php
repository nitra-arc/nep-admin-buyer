<?php

namespace Nitra\BuyerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Bundle\MongoDBBundle\Validator\Constraints\Unique as ODMUnique;

/**
 * @ODM\Document 
 * @ODMUnique(fields="email")
 * @ODMUnique(fields="phone")
 */
class Buyer
{
    /**
     * @ODM\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @ODM\Int
     */
    private $tetradkaId;

    /**
     * @ODM\String 
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ODM\Field(type="string")
     * @Assert\Choice(choices = {"active", "passive"})
     */
    private $status;

    /**
     * @ODM\String
     * @Assert\Email
     */
    private $email;

    /**
     * @ODM\String
     */
    private $phone;

    /**
     * @ODM\String
     */
    private $socialNet;

    /**
     * @ODM\String
     */
    private $socialId;

    /**
     * Уведомлять ли о новых статьях
     * @ODM\Boolean
     */
    private $report_new_information;

    /**
     * Массив товаров, о поступлении которых необходимо сообщить
     * @ODM\Hash
     */
    private $report_products = array();

    /**
     * @ODM\String
     */
    private $cityName;

    /**
     * @ODM\Int
     */
    private $cityId;

    /**
     * @ODM\String
     * @Assert\Length(max="255")
     */
    private $address;

    /**
     * @ODM\String
     */
    private $password;

    public function __toString()
    {
        $d = ($this->email) ? ' - ' : '';
        return $this->name . $d . $this->email;
    }

    public function getStatusView()
    {
        if ($this->status == 'active') {
            $this->statusView = "Активный";
        } elseif ($this->status == 'passive') {
            $this->statusView = "Пасивный";
        } else {
            $this->statusView = '';
        }
        
        return $this->statusView;
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get tetradkaId
     * @return tetradkaId $tetradkaId
     */
    public function getTetradkaId()
    {
        return $this->tetradkaId;
    }

    /**
     * Set tetradkaId
     * @param string $tetradkaId
     * @return self
     */
    public function setTetradkaId($tetradkaId)
    {
        $this->tetradkaId = $tetradkaId;
        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param int $phone
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = preg_replace('/[^0-9]/', '', $phone);
        return $this;
    }

    /**
     * Get phone
     *
     * @return int $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Получить номер телефона для отображения
     * @return string 
     */
    public function getPhoneDisplay()
    {
        // в зависимости от кол-ва цифр в номере телефона 
        // меняем формат отоюражения
        switch (strlen($this->phone)) {
            // россия
            case 11:
                return preg_replace("/([0-9{1})([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{2})/", "+$1($2)$3-$4-$5", $this->phone);
            // украина 
            case 12:
                return preg_replace("/([0-9{1})([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{2})/", "+$1($2)$3-$4-$5", $this->phone);
                break;
        }

        // вернуть телефор без формата
        return $this->phone;
    }

    /**
     * Set socialNet
     *
     * @param string $socialNet
     * @return self
     */
    public function setSocialNet($socialNet)
    {
        $this->socialNet = $socialNet;
        return $this;
    }

    /**
     * Get socialNet
     *
     * @return string $socialNet
     */
    public function getSocialNet()
    {
        return $this->socialNet;
    }

    /**
     * Set socialId
     *
     * @param string $socialId
     * @return self
     */
    public function setSocialId($socialId)
    {
        $this->socialId = $socialId;
        return $this;
    }

    /**
     * Get socialId
     *
     * @return string $socialId
     */
    public function getSocialId()
    {
        return $this->socialId;
    }

    /**
     * Set reportNewInformation
     *
     * @param boolean $reportNewInformation
     * @return self
     */
    public function setReportNewInformation($reportNewInformation)
    {
        $this->report_new_information = $reportNewInformation;
        return $this;
    }

    /**
     * Get reportNewInformation
     *
     * @return boolean $reportNewInformation
     */
    public function getReportNewInformation()
    {
        return $this->report_new_information;
    }

    /**
     * Set reportProducts
     *
     * @param hash $reportProducts
     * @return self
     */
    public function setReportProducts($reportProducts)
    {
        $this->report_products = $reportProducts;
        return $this;
    }

    /**
     * Get reportProducts
     *
     * @return hash $reportProducts
     */
    public function getReportProducts()
    {
        return $this->report_products;
    }

    /**
     * Set cityName
     *
     * @param string $cityName
     * @return self
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;
        return $this;
    }

    /**
     * Get cityName
     *
     * @return string $cityName
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * Set cityId
     *
     * @param int $cityId
     * @return self
     */
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;
        return $this;
    }

    /**
     * Get cityId
     *
     * @return int $cityId
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * Set address
     * @param string $address
     * @return self
     */
    public function setAddress($address)
    {
        $this->ddress = $address;
        return $this;
    }

    /**
     * Get address
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get password
     *
     * @return string $password
     */
    public function getPassword()
    {
        return $this->password;
    }
}