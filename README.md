# ActionManagementBundle

## Описание

данный бандл предназначен для:

* **для создания и редактирования пользователей**

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-admin-buyerbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\BuyerBundle\NitraBuyerBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

* **routing.yml**

```yml
NitraBuyerBundle:
    resource:    "@NitraBuyerBundle/Resources/config/routing.yml"
    prefix:      /{_locale}/
    defaults:
        _locale: ru
    requirements:
        _locale: en|ru
```

* **menu.yml**

```yml
buyer:
    translateDomain: 'menu'
    route: Nitra_BuyerBundle_Buyer_list
#...
tree: #menu containers
        main: #main container
            type: navigation # menu type id
            children:
               buyer: ~
               #...
```