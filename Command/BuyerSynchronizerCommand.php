<?php

namespace Nitra\BuyerBundle\Command;

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Nitra\BuyerBundle\Document\Buyer;

class BuyerSynchronizerCommand extends NitraContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nitra:buyers:synchronize')
            ->setDescription('Loading buyers from tetradka and synchronize him with MongoDB');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $buyers = $this->getTetradkaBuyers();
        
        if (is_array($buyers)) {
            $output->writeln("\nLoading buyers {$this->formatBoldRed('from')} tetradka:");
            $existsWithTId      = 0;
            $existsWithoutTId   = 0;
            $new                = 0;
            
            $progress    = $this->getHelperSet()->get('progress');
            $progress->start($output, count($buyers));
            
            foreach ($buyers as $id => $buyer) {
                $progress->advance();
                $Buyer = $this->findBuyer($buyer);
                if ($Buyer) {
                    if ($Buyer->getTetradkaId() == $id) {
                        $existsWithTId ++;
                    } else {
                        $existsWithoutTId ++;
                    }
                } else {
                    $Buyer = new Buyer();
                    $new ++;
                }
                if (key_exists('email', $buyer)) {
                    $Buyer->setEmail($buyer['email']);
                }
                $Buyer->setPhone(       key_exists('phone', $buyer)     ? $buyer['phone']       : null);
                $Buyer->setName(        key_exists('name', $buyer)      ? $buyer['name']        : null);
                $Buyer->setCityId(      key_exists('city_id', $buyer)   ? $buyer['city_id']     : null);
                $Buyer->setCityName(    key_exists('city_name', $buyer) ? $buyer['city_name']   : null);
                $Buyer->setAddress(     key_exists('address', $buyer)   ? $buyer['address']     : null);
                $Buyer->setTetradkaId($id);
                
                $this->getDocumentManager()->persist($Buyer);
                $this->getDocumentManager()->flush($Buyer);
            }
            $progress->finish();
            
            $output->writeln(
                  "    Loading from tetradka finished successfully."
                . "\n         Inserted " . $this->formatBoldRed($new)
                . "\n         Joined with ORM " . $this->formatBoldRed($existsWithoutTId)
                . "\n         Exists with right ORM buyer ID " . $this->formatBoldRed($existsWithTId)
            );
            
            $output->writeln("\nLoading buyers {$this->formatBoldRed('to')} tetradka:");
            $qb = $this->getDocumentManager()->createQueryBuilder('NitraBuyerBundle:Buyer');
            $qb ->addAnd($qb->expr()->field('password')->exists(true))
                ->addAnd($qb->expr()->field('name')->notEqual(null))
                ->addAnd($qb->expr()->field('phone')->notEqual(null))
                ->addAnd($qb->expr()->field('password')->notEqual(null));
            $Buyers = $qb->getQuery()->getIterator();
            $buyerToTetradka = array(
                'buyers'    => array(),
            );
            $progress->start($output, $Buyers->count());
            foreach ($Buyers as $Buyer) {
                $buyerToTetradka['buyers'][$Buyer->getId()] = $this->formatBuyerToTetradkaArray($Buyer);
                $progress->advance();
            }
            $progress->finish();
            $output->writeln("    Persisting to tetradka...");
            
            $result = $this->setTetradkaBuyers($buyerToTetradka);
                
            $output->writeln(
                  "    Loading to tetradka finished successfully."
                . "\n         Inserted " . $this->formatBoldRed($result['inserted'])
                . "\n         Updated " . $this->formatBoldRed($result['updated'])
                . "\n         Failed " . $this->formatBoldRed($result['failed'])
            );
        } else {
            $hostName = $this->getContainer()->hasParameter('tetradka') ? $this->getContainer()->getParameter('tetradka') : 'localhost';
            $output->writeln(sprintf('Sorry there were errors with getting buyers from tetradka (check: %s).', $this->formatBoldRed(sprintf('http://%s/order/buyers-synchronizer', $hostName))));
        }
    }

    /**
     * @param Buyer $Buyer
     */
    protected function formatBuyerToTetradkaArray($Buyer)
    {
        return array(
            'tetradkaId'        => $Buyer->getTetradkaId(),
            'name'              => $Buyer->getName(),
            'phone'             => $Buyer->getPhone(),
            'email'             => $Buyer->getEmail(),
            'city'              => array(
                'id'                => $Buyer->getCityId(),
                'name'              => $Buyer->getCityName()
            ),
            'address'           => $Buyer->getAddress(),
            'reportNewArticles' => $Buyer->getReportNewInformation(),
            'reportProducts'    => $Buyer->getReportProducts(),
            'status'            => $Buyer->getStatus(),
            'social'            => array(
                'id'                => $Buyer->getSocialId(),
                'net'               => $Buyer->getSocialNet(),
            ),
        );
    }
    
    /**
     * Поиск пользователя по номеру телефона и/или e-mail адресу
     * @param array $buyer
     * @return \Nitra\BuyerBundle\Document\Buyer|null
     */
    protected function findBuyer($buyer)
    {
        $Buyer = null;
        if (key_exists('phone', $buyer) && key_exists('email', $buyer)) {
            $Buyer = $this->getDocumentManager()->createQueryBuilder('NitraBuyerBundle:Buyer')
                ->field('phone')->equals($buyer['phone'])
                ->field('email')->equals($buyer['email'])
                ->getQuery()->execute()->getSingleResult();
        }
        if (!$Buyer && key_exists('phone', $buyer)) {
            $Buyer = $this->getDocumentManager()->createQueryBuilder('NitraBuyerBundle:Buyer')
                ->field('phone')->equals($buyer['phone'])
                ->getQuery()->execute()->getSingleResult();
        }
        if (!$Buyer && key_exists('email', $buyer) && $buyer['email']) {
            $Buyer = $this->getDocumentManager()->createQueryBuilder('NitraBuyerBundle:Buyer')
                ->field('email')->equals($buyer['email'])
                ->getQuery()->execute()->getSingleResult();
        }
        
        return $Buyer;
    }
    
    /**
     * Получение пользователей из тетрадки
     * @return array
     */
    protected function getTetradkaBuyers()
    {
        return $this->tetradka('order/buyers-synchronizer');
    }
    
    /**
     * Получение пользователей из тетрадки
     * @param array $buyers
     * @return array
     */
    protected function setTetradkaBuyers($buyers)
    {
        return $this->tetradka('order/buyers-synchronizer', http_build_query($buyers));
    }
    
    /**
     * @param string        $path
     * @param array|null    $data
     * @return array|null
     */
    protected function tetradka($path, $data = null)
    {
        $host = $this->getContainer()->hasParameter('tetradka') ? $this->getContainer()->getParameter('tetradka') : 'localhost';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://$host/$path");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $cRresponse = curl_exec($ch);
        curl_close($ch);

        return json_decode($cRresponse, true)
            ? json_decode($cRresponse, true)
            : $cRresponse;
    }
}