<?php

namespace Nitra\BuyerBundle\Form\Type\Buyer;

use Admingenerated\NitraBuyerBundle\Form\BaseBuyerType\NewType as BaseNewType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewType extends BaseNewType
{
    protected $options;
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->options = $options;
        
        parent::buildForm($builder, $options);
    }
    
    protected function getFormOption($name, array $formOptions)
    {
        switch ($name) {
            case 'phone':
                $formOptions['mask'] = $this->options['phone_mask'];
                break;
        }
        
        return $formOptions;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'phone_mask'    => '+38(999)999-99-99',
        ));
    }
}