<?php

namespace Nitra\BuyerBundle\Controller\Buyer;

use Admingenerated\NitraBuyerBundle\BaseBuyerController\NewController as BaseNewController;

class NewController extends BaseNewController
{
    protected function getFormOptions(\Nitra\BuyerBundle\Document\Buyer $Buyer)
    {
        return array(
            'phone_mask'    => $this->container->getParameter('nitra_buyer.phone_mask'),
        );
    }
}