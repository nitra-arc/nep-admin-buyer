<?php

namespace Nitra\BuyerBundle\Controller\Buyer;

use Admingenerated\NitraBuyerBundle\BaseBuyerController\EditController as BaseEditController;

class EditController extends BaseEditController
{
    protected function getFormOptions(\Nitra\BuyerBundle\Document\Buyer $Buyer)
    {
        return array(
            'phone_mask'    => $this->container->getParameter('nitra_buyer.phone_mask'),
        );
    }
}