<?php

namespace Nitra\BuyerBundle\Controller\Buyer;

use Admingenerated\NitraBuyerBundle\BaseBuyerController\ListController as BaseListController;

class ListController extends BaseListController
{
    protected function getFilterForm()
    {
        return $this->createForm($this->getFiltersType(), $this->getFilters(), array(
            'phone_mask'    => $this->container->getParameter('nitra_buyer.phone_mask'),
        ));
    }
}